import React from 'react'
import TodoItem from '../TodoItem/TodoItem'
import toggleItem from '../../App.js'


function TodoList ({todos, toggleItem, handleDelete}) {
    return (
      <section className="main">
        <ul className="todo-list">
          {todos.map((todo) => (
            <TodoItem 
            key={todo.title} 
            handleDelete={handleDelete}
             toggleItem={toggleItem}
             todo={todo} 
             />
          ))}
        </ul>
      </section>
    );
  }

  export default TodoList;