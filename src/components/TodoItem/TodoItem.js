import React from 'react'


export default function TodoItem ({todo, toggleItem, handleDelete}) {
  const {completed, id, title} = todo;
  return (
      <li className={completed ? "completed" : ""}>
        <div className="view">
          <input 
          className="toggle" 
          type="checkbox" 
          checked={completed} 
          onClick={() => toggleItem(id)} 
          />
          <label>{title}</label>
          <button className="destroy" 
          onClick={() => handleDelete(id)}/>
        </div>
      </li>
    );
  }