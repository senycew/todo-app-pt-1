import React from "react";
import todosList from "./todos.json";
import TodoList from "./components/ToDoList/ToDoList"

function App () {
const [todos, setTodos] = React.useState(todosList)
const [title, setTitle] = React.useState("")

const handleChange = (evt) => {
  if(evt.keyCode===13) {

    const idName = todos.length + 1
    const newToDo = todos.concat({
      userId: 1,
      id: {idName}, 
      title: `${evt.target.value}`,
      completed: false,
    })
    setTodos(newToDo)
    evt.target.value=""
  }
}
const toggleItem=(id) => {
  const updatedTodos = todos.map((todo) => {
    if (id === todo.id) { 
      return {...todo, completed: !todo.completed};
    }
    return todo;
  });
  setTodos(updatedTodos);
}


function handleDelete (id) {
const updatedTodos = todos.filter((todo) => {
  const sameItem = id !==todo.id;
  return sameItem
});
setTodos(updatedTodos)
}

function clearCompleted (id) {
  const updatedTodos = todos.filter((todo) => {
    const sameItem = !todo.completed;
    return sameItem
});
setTodos(updatedTodos)
}


    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo" 
          placeholder="What needs to be done?" 
          autoFocus  
          onKeyDown = {handleChange}
          />
        </header>
        <TodoList
         todos={todos}
         toggleItem={toggleItem}
         handleDelete={handleDelete} />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button 
          className="clear-completed"
          onClick={() => clearCompleted()
          }
          >Clear completed</button>
        </footer>
      </section>
    );
  }



export default App;